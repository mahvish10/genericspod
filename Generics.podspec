Pod::Spec.new do |s|
    s.name             = 'Generics'
    s.version          = '0.1.0'
    s.summary          = 'Generics for complete application'
    s.homepage         = 'https://gitlab.com/mahvish10/genericspod'
    s.license          = { :type => 'MIT', :file => 'LICENSE' }
    s.author           = { 'mahvishsyed10' => 'mahvishsyed15@gmail.com' }
    s.source           = { :git => 'https://gitlab.com/mahvish10/genericspod.git', :branch => "main" }
    s.ios.deployment_target = '14.0'
    s.swift_version = '4.2'
    s.source_files = 'Generics/Classes/**/*'
    s.dependency 'SnapKit'
    s.dependency 'PromiseKit', "~> 6.8"
    s.dependency 'Alamofire', '~> 5.2'
    s.dependency 'SwiftyJSON', '~> 4.0'
end
